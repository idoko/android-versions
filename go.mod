module gitlab.com/idoko/android-versions

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	google.golang.org/genproto v0.0.0-20200122232147-0452cf42e150
	google.golang.org/grpc v1.19.0
)
