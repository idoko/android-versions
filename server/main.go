package main;

import (
	"context"
	pb "gitlab.com/idoko/android-versions"
)

type server struct{}

func (s *server) GetAllVersions(ctx context.Context, r *pb.ListVersionsRequest) (*pb.ListVersionsResponse) {
	result := &pb.ListVersionsResponse
	result.Result = "k"

	msg := fmt.Sprintf("hello world!")
	log.Println(msg)
	return result, nil
}

func main() {
	listener, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("Could not start server: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterListVersionsServiceServer(s, &server{})
	reflection.Register(s)
	if err := s.Serve(listener); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}